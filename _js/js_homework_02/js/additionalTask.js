// -------------- Additional part of task 2. -------------------------
// 'use strict';
let m = +(prompt(`Please, enter min number greater or equal 2!`, ''));
let n = +prompt(`Please, enter max number!`, '');
let i, a, f;
while (Number.isInteger(n) === false || parseInt(m) !== m || m >= n || m < 2) { // --- проверка корректности ввода данных
    console.log(`${m} and ${n} are not valid. Numbers have to be in Integer format, n > m, m >= 2.`);
}
for (i = m; i <= n; i++) {
    f = 0;
    for (a = 2; a <= Math.sqrt(i); a++) {
        if (i % a === 0) { // --- a - делитель
            f++; // --- f - индикатор простого числа
        }
    }
    if (f === 0) {
        console.log(`${i}. YES! The number is simple.`);
    } /*else {
        console.log(`${i}. No! The number is not simple.`);
    }*/
}