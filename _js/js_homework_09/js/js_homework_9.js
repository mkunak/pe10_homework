// Задание
// Реализовать переключение вкладок (табы) на чистом Javascript.
// 
// Технические требования:
// В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный
// текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой
// текст должен отображаться для какой вкладки.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться.
// При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

console.log(` ----- Variant#1. ----- `);
// получаем доступ ко всем элементам в меню и блоке с контентом:
const tabsCollection = document.querySelectorAll(".tabs-title"); //console.log(tabsCollection);
const tabsContent = document.querySelector(".tabs-content"); //console.log(tabsContent);
const tabsContentItemCollection = tabsContent.children; //console.log(tabsContentItemCollection);
// теперь производим манипуляции с классами, особенно с классом active
for (let i = 0; i < tabsCollection.length; i++) {
    tabsContentItemCollection[i].classList.add(`tabs-content-item`); // подключаем класс к элементам контента,
    // который их скроет (потом они будут появляться по событию клик на соответсвтующем элементе меню)
    tabsCollection[i].addEventListener('click', function () {
        tabsCollection[i].classList.add(`active`); // пункт меню окрашивается в цвет нажатия
        tabsContentItemCollection[i].classList.add(`active`); // показываем текст на странице при нажатии на пункт меню
        for (let j = 0; j < tabsCollection.length; j++) { // в этом цикле удаляем лишние active
            if (j !== i) {
                tabsCollection[j].classList.remove(`active`);
                tabsContentItemCollection[j].classList.remove(`active`);
            }
        }
    });
}