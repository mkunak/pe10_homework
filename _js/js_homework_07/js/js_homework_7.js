// Теоретический вопрос:
// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)

// Задание:
// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка.
// Технические требования:
// Создать функцию, которая будет принимать на вход массив.
// Каждый из элементов массива вывести на страницу в виде пункта списка
// Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка перед выведением его на страницу.
// Примеры массивов, которые можно выводить на экран:
// ['hello', 'world', 'Kyiv', 'Lviv', 'Kharkiv', 'Odesa']
// ['1', '2', '3', 'sea', 'user', 23]
// Можно взять любой другой массив.

// Не обязательное задание продвинутой сложности:
// ??? Очистить страницу через 10 секунд. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.
// Если внутри массива одним из элементов будет еще один массив или объект, выводить его как вложенный список.
//
// Ответ на теоритический вопрос:
// ДОМ - это составная часть БОМ, корневым узлом которой является <!doctype html>. ДОМ является интерфейсом между JS
// и html/css.

function makeList(array) {
    let makeNewDiv = document.createElement("div");
    makeNewDiv.className = `randomList`;
    makeNewDiv.id = `randomList`;
    document.body.appendChild(makeNewDiv);

    let makeNewUl = document.createElement("ul");
    makeNewUl.innerHTML = `The list of random words:`;
    makeNewUl.className = `listClass`;
    makeNewUl.id = `listId`;
    document.getElementById('randomList').appendChild(makeNewUl);

    /*for (let i = 0; i < array.length; i++) {
        if (!Array.isArray(array[i])) {
            makeNewLi = document.createElement("li");
            makeNewLi.innerHTML = array[i];
            listId.appendChild(makeNewLi);
        }
        if (Array.isArray(array[i])) {
            makeNewLi.id = `listItemId`;
            for (let j = 0; j < array[i].length; j++) {
                makeNewLi = document.createElement("li");
                makeNewLi.innerHTML = array[i][j];
                listItemId.appendChild(makeNewLi);
            }
        }
    }*/

    let makeNewLi;
    array.map(function f(value) {
        if (Array.isArray(value)) {
            makeNewLi.id = `listItemId`;
// можно еще красивее
// вынести создание div и ul за пределы функции
// а внутри map если это массив, не делай еще один map, а вызови функцию рекурсивно
            value.map(function (value) {
                makeNewLi = document.createElement("li");
                makeNewLi.innerHTML = value;
                listItemId.appendChild(makeNewLi);
            });
        }
        else {
            makeNewLi = document.createElement("li");
            makeNewLi.innerHTML = value;
            listId.appendChild(makeNewLi);
        }
    });

    console.log(makeNewUl, typeof array);
    return makeNewDiv;
}

let moveStep = 0;

function counter(seconds, elementId) {
    document.getElementById(elementId).innerText = `It lost only ${seconds} seconds`;
    let timerId = 0;
    if (seconds < 0) {
        clearTimeout(timerId);
        document.body.parentNode.removeChild(document.body);
    }
    seconds--;
    timerId = setTimeout(function () {
        counter(seconds, elementId);
    }, 1000);
    document.getElementById(`sqr`).style.marginTop = moveStep + `px`;
    moveStep += 50;
    document.getElementById(`btnStop`).onclick = function () {
        clearTimeout(timerId);
    }
}

const arrayOfRandomWords = ['hello', 'world', 'Kyiv', 'Lviv', ['2', '3', 'user', 23], 'Odesa'];
document.getElementById(`randomText`).innerHTML = `${arrayOfRandomWords}`;
// let arrayOfRandomWords = document.getElementById(`randomText`).innerText.split(` `);
console.log(arrayOfRandomWords);

makeList(arrayOfRandomWords);
counter(10, "timer");