// Задание
// Добавить в домашнее задание HTML/CSS №6 (Flex/Grid) различные эффекты с использованием jQuery.
//
// Технические требования:
// 1 - Добавить вверху страницы горизонтальное меню со ссылкой на все разделы страницы.
// 2 - При клике на ссылку - плавно прокручивать страницу вниз до выбранного места.
// 3 - Если прокрутить страницу больше чем на 1 экран вниз, справа внизу должна появляться кнопка "Наверх" с
// фиксированным позиционариванием. При клике на нее - плавно прокрутить страницу в самый верх.
// 4 - Добавить под одной из секций кнопку, которая будет выполнять функцию slideToggle() (прятать и показывать
// по клику) для данной секции.
//
// Литература:
// Якоря
// jQuery для начинающих
// Стандартные анимации jQuery

console.log(` -------------------- JS_Homework#15. Architect with JS-actions --------------------`);

const navMenuLinksCollection = $(`.pageNavMenuItem a`);//console.log(navMenuLinksCollection);
const goTop = $("a[href='#home']");

$(document).ready(function () {

    $(`.navigationBtn`).click(function () {
        $(`#pageNavigationMenu`).toggleClass(`active`);
    });

    $(`.shutDown`).click(function () {
        $(`#pageNavigationMenu`).removeClass(`active`);
    });

    for (let i = 0; i < navMenuLinksCollection.length; i++) {
        $(navMenuLinksCollection.eq(i)).click(function () { //console.log(this);
            $("html, body").animate({
                scrollTop: $($(this).attr("href")).offset().top
            }, {
                duration: 1000,
            });
        });
    }
    //scrollTo(pageX,pageY)
    goTop.click(function () { // scrollTo(pageX,pageY)
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });

    $(".block-i").hide();
    $(".block-i").slice(0, 2).show();
    $("#showMoreBtn").click(function () {
        if ($("#showMoreBtn").html() === `Show more`) {
            $(`.block-i:hidden`).slice(0, 2).fadeIn(1000);
            if ($(`.block-i:hidden`).length === 0) {
                $("#showMoreBtn").html('Hide images');
                $("#showMoreBtn").removeClass(`buttonStyleGreen`).addClass(`buttonStyleBlue`);
            }
        } else if ($("#showMoreBtn").html() === `Hide images`) {
            $(".block-i").hide();
            $(".block-i").slice(0, 2).show();
            $("#showMoreBtn").html('Show more');
            $("#showMoreBtn").removeClass(`buttonStyleBlue`).addClass(`buttonStyleGreen`);
        }
    });
});

window.onscroll = function () {
    $(`#pageNavigationMenu`).removeClass(`active`);
    if (window.pageYOffset > document.documentElement.clientHeight) {
        $(`.goTop`).addClass(`active`);
    } else {
        $(`.goTop`).removeClass(`active`);
    }
};