// Теоретический вопрос
// Напишите как вы понимаете рекурсию. Для чего она используется на практике?
//
// Задание
// Реализовать функцию подсчета факториала числа.
//
// Технические требования:
//
// Считать с помощью модального окна браузера число, которое введет пользователь.
// С помощью функции посчитать факториал числа, которое ввел пользователь, и вывести его на экран.
// Использовать синтаксис ES6 для работы с перемеными и функциями.
//
// Не обязательное задание продвинутой сложности:
//
// После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо
// при вводе их указал не числа, - спросить оба числа заново (при этом значением по умолчанию
// для каждой из переменных должна быть введенная ранее информация).

// Solution:

function countFactorial(_number) { // в параметр функции "factorial" помещается аргумент. Название параметра технического значения не имеет. Это название может совпадать с названием переменной-аргументом.
    if (_number < 1) { // проверка значения параметра.
        return 1;
    }
    return _number * countFactorial(_number - 1); // рекуррентный вызов функции "factorial". Функция вызывает сама себя, пока условие (_number < 1) == false.
}

/*function isMax(_firstNumber, _secondNumber) { // функция определения максимального числа из двух
    if (_firstNumber > _secondNumber) {
        return `Maximum is ${_firstNumber}. While ${_firstNumber} is lager then ${_secondNumber}`;
    } else {
        return `Maximum is ${_secondNumber}. While ${_secondNumber} is lager then ${_firstNumber}`;
    }
}*/

let firstNumber = Number(prompt(`Please, enter first number, you like!`, ``));
while (isNaN(firstNumber) || firstNumber === undefined || firstNumber === null) {
    firstNumber = Number(prompt(`Please, enter a number, you like!`, `${firstNumber}`));
}

let secondNumber = Number(prompt(`Please, enter second number, you like!`, ``));
while (isNaN(secondNumber) || secondNumber === undefined || secondNumber === null) {
    secondNumber = Number(prompt(`Please, enter a number, you like!`, `${secondNumber}`));
}

document.write(`First factorial is ${countFactorial(firstNumber)}.`); //вызов функции "factorial" происходит с передачей аргумента. Вычисляем и выводим факториал первого числа.
document.write(`\nSecond factorial is ${countFactorial(secondNumber)}.`); //вызов функции "factorial" происходит с передачей аргумента. Вычисляем и выводим факториал второго числа.
// document.write(isMax(firstNumber, secondNumber)); // Определяем и выводим максимальное из двух