// #Теоретический вопрос:
// Опишите своими словами разницу между функциями setTimeout() и setInterval().
// Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
// Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?
//
// Задание:
// Реализовать программу, показывающую циклично разные картинки.
//
// Технические требования:
// В папке banners лежит HTML код и папка с картинками.
// При запуске программы на экране должна отображаться первая картинка.
// Через 10 секунд вместо нее должна быть показана вторая картинка.
// Еще через 10 секунд - третья.
// Еще через 10 секунд - четвертая.
// После того, как покажутся все картинки - этот цикл должен начаться заново.
// При запуске программы где-то на экране должна появиться кнопка с надписью Прекратить.
// По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка, которая была
// там при нажатии кнопки.
// Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл продолжается с
// той картинки, которая в данный момент показана на экране.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
//
// Не обязательное задание продвинутой сложности:
// При запуске программы на экране должен быть таймер с секундами и миллисекундами, показывающий сколько осталось
// до показа следующей картинки.
// Делать скрытие картинки и показывание новой картинки постепенным (анимация fadeOut / fadeIn) в течение 0.5 секунды.
//
// Литература:
// setTimeout и setInterval

`use strict`;
console.log(` -------------------- JS_Homework#12. setTimout/setInterval. Slider -------------------- `);

// 1 - формируем исходные данные, с которыми будем работать - коллекции, переменные, константы:
const btnsCollection = document.querySelectorAll(`.btn`); //console.log(btnsCollection);
const imgsCollection = document.querySelectorAll(`.image-to-show`); //console.log(imgsCollection[1]);
const framesCollection = document.querySelectorAll(`.frame`); //console.log(imgsCollection[1]);
const mSeconds = 100;
let secondsFromUser = 10; // время показа одного слайда, пользователь может ввести свое значение
let secondsFromFunction = 0, msFromFunction = 0;
let timerIdImg = 0, timerIdCounter = 0;
let i = 0;
let test = 0;

// 2 - кнопке Стоп присвоить очистку ф-ции setTimeout через переменную timerIdImg и timerIdCounter.
btnsCollection[0].addEventListener(`click`, function () {
    clearTimeout(timerIdImg);
    clearTimeout(timerIdCounter);
});

// console.log(timerIdImg === timerIdCounter); // true !!??
// console.log(timerIdImg === test); // true !!??

// 3 - кнопке Воспроизведение снова присваиваем ф-цию counter, которая продолжит отсчет времени с того момента,
// на котором сработала кнопка Стоп, благодаря двум переменным - secondsFromFunction и msFromFunction:
btnsCollection[1].addEventListener(`click`, function () {
    if(clearTimeout(timerIdImg)) {
        counter(secondsFromFunction, `counter`, msFromFunction);
    } else {
        clearTimeout(timerIdImg);
        clearTimeout(timerIdCounter);
        counter(secondsFromFunction, `counter`, msFromFunction);
    }
});
// 4 - вызываем функцию counter, которой передаем исходные данные. Внутри counter будет вызываться и функция show(),
// которая присваивает/удаляет CSS-свойство show, определяющее видимое состояние текущей картинки.
counter(secondsFromUser, `counter`, mSeconds);
function showImg() {
        imgsCollection[i].classList.remove(`show`); // у предыдущего удаляем класс show
        framesCollection[i].classList.remove(`show`); // у предыдущего удаляем класс show
    // $(`.image-to-show`).eq(i).fadeOut(500);
    // $(`.frame`).eq(i).fadeOut(500);
    i++;
    if (i === imgsCollection.length) {
        i = 0;
    }
    test = setTimeout(function () { // здесь setTimeout нужен, чтобы успела скрыться предыдущая картинка
        // $(`.image-to-show`).eq(i).fadeIn(500);
        // $(`.frame`).eq(i).fadeIn(500);
            imgsCollection[i].classList.add(`show`); // поочереди присваиваем класс show
            framesCollection[i].classList.add(`show`); // поочереди присваиваем класс show
    }, 500);
}

function counter(seconds, elementId, mSeconds) {
    document.getElementById(elementId).children[0].innerHTML = `Next slide comes in `;
    document.getElementById(elementId).children[1].innerHTML = `${seconds - 1}`;
    document.getElementById(elementId).children[2].innerHTML = `:`;
    document.getElementById(elementId).children[3].innerHTML = `${mSeconds}`;
    if (seconds < 1) {
        clearTimeout(timerIdCounter);
        seconds = secondsFromUser;
        showImg();
    }
    if (mSeconds < 1) {
        seconds--;
        mSeconds = 100;
    }
    mSeconds--;
    timerIdCounter = setTimeout(function () {
        secondsFromFunction = seconds;
        msFromFunction = mSeconds;
        counter(seconds, elementId, mSeconds);
    }, 10);
}