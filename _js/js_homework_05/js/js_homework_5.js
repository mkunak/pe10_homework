// JS_Homework#5. User age and password.
//
// Теоретический вопрос:
// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования.
//
// Задание:
// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
// Технические требования:
// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее
// следующим функционалом:
// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить
// ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем
// регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
//
// Ответ на теоретический вопрос:
// Экранирование применяется в том, случае, когда какой-либо символ кода (применяющийся как служебный в языке
// программирования) должен быть представлен в качестве обычного текста.

console.log('----------------------------- JS_Homework#5. User age and password.-----------------------------');

function createNewUser() {
    let _firstName = 'mykola';//prompt(`What is your First Name?`, ``);
    let _secondName = 'kunak';//prompt(`What is your Second Name?`, ``);
    let _birthdayArray = '14.04.1981'.split('.');//prompt(`What is your birth date? Enter format is dd.mm.yyyy`, `dd.mm.yyyy`).split('.');
    let _birthday = new Date(Number(_birthdayArray[2]), Number(_birthdayArray[1]) - 1, Number(_birthdayArray[0]));
    let newUser = {
        firstName: _firstName,
        secondName: _secondName,
        birthday: _birthday
    };
    newUser.getLogin = function () {
        return (this.firstName.charAt(0) + this.secondName).toLowerCase();
    };
    newUser.getAge = function () {
        let now = new Date();
        let age = now.getFullYear() - this.birthday.getFullYear();
        if (now.getMonth() > this.birthday.getMonth()) {
            return `User age is ${age}`;
        } else if (now.getMonth() === this.birthday.getMonth() && now.getDate() >= this.birthday.getDate()) {
            return `User age is ${age}`;
        } else {
            return `User age is ${age - 1}`;
        }
    };
    newUser.getPassword = function () {
        return (this.firstName.charAt(0).toUpperCase() + this.secondName.toLowerCase() + this.birthday.getFullYear());
    };
    return newUser;
}

let newUser = createNewUser();
// console.log(newUser.getLogin());
console.log(newUser);
console.log(newUser.getPassword());
console.log(newUser.getAge());
console.log('----------------------------- JS_Homework#5. User age and password.-----------------------------');